/**
* Created by Tom on 11/02/2016.
* Project PersonalWebsite.
* See @ http://jsfiddle.net/a8yJt/
* See @ http://jsfiddle.net/hsJbu/42/
*/
$(document).ready(function()
{
    $(".divPanels div").each(function(e)
    {
        if (e != 0)
            $(this).hide();
    });

    $(".btnNext").click(function ()
    {
        if ($(".divPanels div:visible").next().length != 0)
        {
            $(".divPanels div:visible").fadeOut(function()
            {
                $(this).next().fadeIn();
            });
        }
        else
        {
            $(".divPanels div:visible").fadeOut(function()
            {
                $(".divPanels div:first").fadeIn();
            });
        }
        return false;
    });

    $(".btnPrev").click(function ()
    {
        if ($(".divPanels div:visible").prev().length != 0)
        {
            $(".divPanels div:visible").fadeOut(function()
            {
                $(this).prev().fadeIn();
            });
        }
        else
        {
            $(".divPanels div:visible").fadeOut(function()
            {
                $(".divPanels div:last").fadeIn();
            });
        }
        return false;
    });
});